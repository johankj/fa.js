/**
 * Representation of an alphabet.
 * 
 * An alphabet is represented by a non-empty set of characters.
 * For simplificy, alphabets should not contain the characters
 * '#', '%', '+', '*', '(', or ')', which have special meaning in regular expresions.
 */

/**
 * Constructs an alphabet consisting of the given symbols.
 * @param symbols non-empty string of symbols
 * @exception IllegalArgumentException if symbols is empty
 */
export default function Alphabet(addsymbols) {
  if (!addsymbols || addsymbols.length === 0) {
    throw new Error("An alphabet cannot be empty");
  }
  this.symbols = new Set(); // Set of Characters

  addsymbols.split("").forEach( symbol => {
    this.symbols.add(symbol);
  });
  this._checkSpecialChars();
}

/**
 * Checks that special characters do not occur in the alphabet.
 * This method is used by the constructors.
 *
 * @private
 */
Alphabet.prototype._checkSpecialChars = function() {
  if (this.symbols.has('#') ||
      this.symbols.has('%') ||
      this.symbols.has('+') ||
      this.symbols.has('*') ||
      this.symbols.has('(') ||
      this.symbols.has(')'))
  {
    throw new Error("#, %, +, *, (, and ) are not allowed in alphabets");
  }
};

/**
 * Returns the number of symbols in this alphabet.
 */
Alphabet.prototype.numberOfSymbols = function() {
    return this.symbols.size;
};

/**
 * Checks whether the given alphabet is the same as this one.
 */
Alphabet.prototype.equals = function(obj) {
  if (!(obj instanceof Alphabet))
    return false;
  return [...this.symbols].sort().toString() == [...obj.symbols].sort().toString();
}
