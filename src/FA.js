/**
 * A state. Identified by its name.
 */
export function State(name) {
  return { name };
}

/**
 * A pair of State and Symbol.
 */
function StateSymbolPair(state, char) {
  return { state, char };
}

/**
 * Pair of states. Used in product construction and in construction of regular expression.
 */
function StatePair(s1, s2) {
  return { s1, s2 };
}

/**
 * Helper method to return objects rather than strings.
 */
function unstringify(iterator) {
  return {
    [Symbol.iterator]: function*() {
      for (let stringified of iterator) {
        yield JSON.parse(stringified);
      }
    }
  };
}

export default function FA(a) {
  this._states = new Set(); // Set of states
  this._accept = new Set(); // Set of states
  this._alphabet = a;
  this._initial = null;
  this._transitions = new Map(); // Map of <StateSymbolPair, State>
}

/**
 * Returns the number of transitions
 */
FA.prototype.numberOfTransitions = function() {
  return this._transitions.size;
};

/**
 * Returns the transitions as [StateSymbolPair, State] or [{ State, char }, State]
 */
FA.prototype.transitions = function() {
  var self = this;
  return {
    [Symbol.iterator]: function*() {
      for (let [key, value] of self._transitions) {
        yield [JSON.parse(key), JSON.parse(value)];
      }
    }
  };
};

/**
 * Returns the states as an generator.
 */
FA.prototype.states = function() {
  return unstringify(this._states);
};

/**
 * Returns the number of states.
 */
FA.prototype.numberOfStates = function() {
  return this._states.size;
};

/**
 * Add states to the FA.
 * @param {State} [states]
 */
FA.prototype.addStates = function(...states) {
  for (let state of states) {
    this._states.add(JSON.stringify(state));
  }
};

/**
 * Add/set a transaction from State q via symbol c to State p.
 * @param {State} q
 * @param {char) c
 * @param {State} p
 */
FA.prototype.setTransition = function(q, c, p) {
  if (!this._states.has(JSON.stringify(q)) || !this._states.has(JSON.stringify(p)))
    throw new Error('State in transition is not part of FA');
  this._transitions.set(JSON.stringify(StateSymbolPair(q, c)), JSON.stringify(p));
};

/**
 * Returns the initial state.
 */
FA.prototype.initialState = function() {
  return this._initial;
};

/**
 * Sets the initial state.
 * @param {State} q
 */
FA.prototype.setInitialState = function(q) {
  this._initial = q;
};

/**
 * Returns the state 
 * @param {State} q
 * @param {char} c
 */
FA.prototype.delta = function(q, c) {
  if (!this._alphabet.symbols.has(c))
    throw new Error(`Symbol '${c}' not in alphabet`);
  q = this._transitions.get(JSON.stringify(StateSymbolPair(q, c)));
  return q ? JSON.parse(q) : null;
};

/**
 * Performs transitions in extended transition function. [Martin, Def. 2.12]
 * @return \delta*(q,s)
 * @exception If a symbol in 'str' is not in the alphabet
 */
FA.prototype.deltaStar = function(q, str) {
  // (Using recursion instead of iteration would have been closer to
  // the mathematical definition, but this code is simpler...)
  for (let char of str)
    q = this.delta(q, char);
  return q;
};

/**
 * Add states to the set of accepted states.
 * @param {State} [states]
 */
FA.prototype.addAcceptStates = function(...states) {
  for (let state of states) {
    this._accept.add(JSON.stringify(state));
  }
};

/**
 * Remove states from the set of accepted states.
 * @param {State} [states]
 */
FA.prototype.removeAcceptStates = function(...states) {
  for (let state of states) {
    this._accept.delete(JSON.stringify(state));
  }
};

/**
 * Runs the given string on this automaton. [Martin, Def. 2.14]
 * @param {str} A string to test
 * @return true iff the string is accepted
 * @exception If a symbol in 'str' is not in the alphabet
 */
FA.prototype.accepts = function(str) {
  let endState = this.deltaStar(this._initial, str);
  return endState ? this._accept.has(JSON.stringify(endState)) : false;
};

/**
 * Finds the set of states that are reachable from the initial state. [Martin, Exercise 2.27(b)]
 */
FA.prototype.findReachableStates = function() {
  let reachable = new Set(); // Contains the states that are reachable and have been visited
  let pending = new Set(); // Contains the states that are reachable but have not yet been visited
  pending.add(this._initial);
  while (pending.size !== 0) {
    let state_q = pending.values().next().value;
    pending.delete(state_q);
    reachable.add(JSON.stringify(state_q));
    for (let a of this._alphabet.symbols) {
      let state_p = this.delta(state_q, a);
      if (state_p !== null && !reachable.has(JSON.stringify(state_p)))
        pending.add(state_p);
    }
  }
  return unstringify(reachable);
};

/**
 * Constructs a new automaton with the same language as this automaton but without unreachable states.
 * The input automaton is unmodified.
 */
FA.prototype.removeUnreachableStates = function() {
  let newFA = this.clone();
  let reachable = [...newFA.findReachableStates()].map(x => x.name);
  // Remove dead states
  for (let state of newFA.states()) {
    if (reachable.indexOf(state.name) == -1) {
      newFA._states.delete(JSON.stringify(state));
      newFA._accept.delete(JSON.stringify(state));
    }
  }
  // Remove transitions from dead states
  for (let [{state, char}, toState] of newFA.transitions()) {;
    if (!newFA._states.has(JSON.stringify(state))) {
      newFA._transitions.delete(JSON.stringify(StateSymbolPair(state, char)));
    }
  }
  return newFA;
};

/**
 * Returns a clone of the FA.
 */
FA.prototype.clone = function() {
  let newFA = new FA(this._alphabet);
  let united_states = new Map(); // Map from old states to new states
  for (let p of this._states) {
    newFA._states.add(p);
    united_states.set(p, p);
    if (this._accept.has(p))
      newFA._accept.add(p);
  }
  newFA._initial = JSON.parse(united_states.get(JSON.stringify(this._initial)));
  for (let [{state, char}, toState] of this.transitions()) {
    let q = JSON.parse(united_states.get(JSON.stringify(state)));
    let p = JSON.parse(united_states.get(JSON.stringify(toState)));
    newFA.setTransition(q, char, p);
  }
  return newFA;
};

/**
 * Constructs a new automaton that accepts the complement of the language of this automaton.
 * The input automaton is unmodified.
 */
FA.prototype.complement = function() {
  let f = this.clone();
  let s = new Set();
  for (let state of f._states)
    s.add(state);
  for (let state of f._accept)
    s.delete(state);
  f._accept = s;
  return f;
};

/**
 * Constructs a new automaton whose language is the intersection of the language of this automaton
 * and the language of the given automaton. [Martin, Th. 2.15]
 * The input automata are unmodified.
 */
FA.prototype.intersection = function(f) {
  return this._product(f, 'intersect');
};

/**
 * Constructs a new automaton whose language is the union of the language of this automaton
 * and the language of the given automaton. [Martin, Th. 2.15]
 * The input automata are unmodified.
 */
FA.prototype.union = function(f) {
  return this._product(f, 'union');
};

/**
 * Constructs a new automaton whose language is equal to the language of this automaton
 * minus the language of the given automaton. [Martin, Th. 2.15]
 * The input automata are unmodified.
 */
FA.prototype.minus = function(f) {
  return this._product(f, 'minus');
};

/**
 * Constructs a new automaton whose language is equal to the language of this automaton
 * with setOperation of the language of the given automaton.
 * The input automata are unmodified.
 */
FA.prototype._product = function(f, setOperation) {
  if (!this._alphabet.equals(f._alphabet))
    throw new Error("The two FAs don't have the same alphabet.");

  let result = new FA();
  result._alphabet = this._alphabet;

  // Create new states
  let stateMap = new Map();
  for (let state1 of this.states()) {
    for (let state2 of f.states()) {
      let pair = StatePair(state1, state2);
      let newState = State(state1.name + state2.name);
      stateMap.set(JSON.stringify(pair), newState);
      result.addStates(newState);
    }
  }

  // Set initial state
  result._initial = stateMap.get(JSON.stringify(StatePair(this._initial, f._initial)));

  // Create new map of transitions
  for (let statePair of stateMap.keys()) {
    let sourceState = stateMap.get(statePair);
    statePair = JSON.parse(statePair);
    for (let char of this._alphabet.symbols) {
      let fa1Target = this.delta(statePair.s1, char);
      let fa2Target = f.delta(statePair.s2, char);
      let resultTarget = stateMap.get(JSON.stringify(StatePair(fa1Target, fa2Target)));
      result.setTransition(sourceState, char, resultTarget);
    }
  }

  // Construct accept-states, depending on the setOperation
  for (let p of this._states) {
    for (let q of f._states) {
      if (setOperation == 'union') {
        if ((this._accept.has(p)) || (f._accept.has(q))) {
          result.addAcceptStates(stateMap.get(JSON.stringify(StatePair(JSON.parse(p), JSON.parse(q)))));
        }
      }
      else if (setOperation == 'minus') {
        if ((this._accept.has(p)) && !(f._accept.has(q))) {
          result.addAcceptStates(stateMap.get(JSON.stringify(StatePair(JSON.parse(p), JSON.parse(q)))));
        }
      }
      else if (setOperation == 'intersect') {
        if ((this._accept.has(p)) && (f._accept.has(q))) {
          result.addAcceptStates(stateMap.get(JSON.stringify(StatePair(JSON.parse(p), JSON.parse(q)))));
        }
      }
    }
  }

  result = result.removeUnreachableStates();
  return result;
};

/**
 * Checks that this automaton is well-defined.
 * In particular, this method checks that the transition function is total.
 * This method should be invoked after each FA-operation during testing.
 * @return this automaton
 * @exception If this automaton is not well-defined
 */
FA.prototype.checkWellDefined = function() {
  if (this._states == null || this._alphabet == null || this._alphabet.symbols == null ||
      this._initial == null || this._accept == null || this._transitions == null)
    throw new Error("AutomatonNotWellDefined: Invalid null pointer");
  if (!this._states.has(JSON.stringify(this._initial)))
    throw new Error("AutomatonNotWellDefined: The initial state is not in the state set");
  if (![...this._accept].every(state => this._states.has(state)))
    throw new Error("AutomatonNotWellDefined: Not all accept states are in the state set");
  for (let state of this.states()) {
    for (let char of this._alphabet.symbols) {
      let s2 = this._transitions.get(JSON.stringify(StateSymbolPair(state, char)));
      if (s2 == undefined)
        throw new Error("AutomatonNotWellDefined: Transition function is not total");
      if (!this._states.has(s2))
        throw new Error("AutomatonNotWellDefined: There is a transition to a state that is not in state set");
    }
  }
  
  for (let [{state, char}, _] of this.transitions()) {
    if (!this._states.has(JSON.stringify(state)))
      throw new Error("AutomatonNotWellDefined: Transition refer to a state not in the state set");
    if (!this._alphabet.symbols.has(char))
      throw new Error("AutomatonNotWellDefined: Non-alphabet symbol appears in transitions");
  }
  return this;
}
