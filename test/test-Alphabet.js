import chai, { assert } from 'chai';
import Alphabet from '../src/Alphabet';

describe('Alphabet', function() {
  it('should not accept string of zero symbols', function() {
    assert.throws(() => new Alphabet(), 'An alphabet cannot be empty');
  });
  it('should accept string of one symbol', function() {
    let alphabet = new Alphabet('1');
    assert.deepEqual([...alphabet.symbols], ['1']);
  });
  it('should accept string of two symbols', function() {
    let alphabet = new Alphabet('01');
    assert.deepEqual([...alphabet.symbols], ['0', '1']);
  });
  it('should accept string of any length', function() {
    // Random length string of numbers from 0 to 9, e.g. '0', '01234'
    let string = Array.from(Array(~~(Math.random()*10)+1).keys()).join('');
    let alphabet = new Alphabet(string);
    assert.equal(alphabet.numberOfSymbols(), string.length);
  });
  it('should not accept special symbols', function() {
    assert.throws(() => new Alphabet('#1'), /are not allowed in alphabets/);
    assert.throws(() => new Alphabet('+'), /are not allowed in alphabets/);
    assert.throws(() => new Alphabet('0*'), /are not allowed in alphabets/);
    assert.throws(() => new Alphabet('(1)'), /are not allowed in alphabets/);
    assert.throws(() => new Alphabet('%'), /are not allowed in alphabets/);
  });
  it('should not contain repeating symbols', function() {
    let alphabet = new Alphabet('0011');
    assert.deepEqual([...alphabet.symbols], ['0', '1']);
    alphabet = new Alphabet('100');
    assert.deepEqual([...alphabet.symbols], ['1', '0']);
    alphabet = new Alphabet('ababcab');
    assert.deepEqual([...alphabet.symbols], ['a', 'b', 'c']);
  });
  it('should check equality of other alphabets', function() {
    let alphabet1 = new Alphabet('01');
    let alphabet2 = new Alphabet('10');
    let alphabet3 = new Alphabet('ab');
    assert.ok(alphabet1.equals(alphabet1));
    assert.ok(alphabet1.equals(alphabet2));
    assert.ok(alphabet2.equals(alphabet1));
    assert.notOk(alphabet1.equals(alphabet3));
    assert.notOk(alphabet3.equals({}));
  });
});
