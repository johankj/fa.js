import chai, { assert } from 'chai';
import FA, { State } from '../src/FA';
import Alphabet from '../src/Alphabet';

describe('FA', function() {
  describe('#constructor', function() {
    it('should have empty values on construction', function() {
      let f = new FA();
      assert.deepEqual([...f._states], []);
      assert.deepEqual([...f._accept], []);
      assert.deepEqual([...f._transitions], []);
      assert.equal(f._initial, null);
      assert.equal(f._alphabet, null);
    });
  });

  describe('#addStates', function() {
    let f;
    beforeEach(function() {
      f = new FA();
    });
    it('should have zero states at start', function() {
      assert.equal(f.numberOfStates(), 0);
    });
    it('should be able to add one state', function() {
      f.addStates(State('A'));
      assert.equal(f.numberOfStates(), 1);
    });
    it('should be able to add multiple independently states', function() {
      f.addStates(State('A'));
      f.addStates(State('B'));
      f.addStates(State('C'));
      assert.equal(f.numberOfStates(), 3);
    });
    it('should be able to add multiple states in one go', function() {
      f.addStates(State('A'), State('B'), State('C'));
      assert.equal(f.numberOfStates(), 3);
    });
  });
  
  describe('#states', function() {
    let f;
    beforeEach(function() {
      f = new FA();
    });
    it('should return iterator of states', function() {
      f.addStates(State('A'), State('B'), State('C'));
      assert.property(f.states(), Symbol.iterator);
      assert.deepEqual([...f.states()], [State('A'), State('B'), State('C')]);
    });
    it('should be able to iterate states', function() {
      f.addStates(State('A'), State('B'), State('C'));
      let i = 0;
      for (let state of f.states()) {
        i++;
        assert.ok(state);
        assert.property(state, 'name');
      }
      assert.equal(i, 3);
    });
  });
  
  describe('#transitions', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('A'), '1', State('A'));
      f.setTransition(State('B'), '0', State('A'));
      f.setTransition(State('B'), '1', State('B'));
    });
    it('should return iterator of transitions', function() {
      assert.property(f.transitions(), Symbol.iterator);
    });
    it('should be able to iterate transitions', function() {
      let i = 0;
      for (let [{ state, char }, toState] of f.transitions()) {
        i++;
        assert.ok(state);
        assert.ok(char);
        assert.ok(toState);
        assert.property(state, 'name');
        assert.typeOf(char, 'string');
        assert.property(toState, 'name');
      }
      assert.equal(i, 4);
    });
  });
  
  describe('#delta', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('A'), '1', State('A'));
      f.setTransition(State('B'), '0', State('A'));
      f.setTransition(State('B'), '1', State('B'));
    });
    it('should throw error when transitioning on a non-alphabet symbol', function() {
      assert.throws(() => f.delta(State('A'), 'z'), "Symbol 'z' not in alphabet");
      assert.doesNotThrow(() => f.delta(State('A'), '0'), /Symbol.*not in alphabet/);
    });
    it('should be able to transition to other states', function() {
      assert.deepEqual(f.delta(State('A'), '0'), State('B'));
      assert.deepEqual(f.delta(State('A'), '1'), State('A'));
      assert.deepEqual(f.delta(State('B'), '0'), State('A'));
    });
    it('should return null when transition doesn\'t exist', function() {
      assert.isNull(f.delta(State('C'), '1'));
    });
  });
  
  describe('#deltaStar', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('A'), '1', State('A'));
      f.setTransition(State('B'), '0', State('A'));
      f.setTransition(State('B'), '1', State('B'));
    });
    it('should throw error when string contains a non-alphabet symbol', function() {
      assert.throws(() => f.deltaStar(State('A'), '010z'), "Symbol 'z' not in alphabet");
      assert.doesNotThrow(() => f.deltaStar(State('A'), '100'), /Symbol.*not in alphabet/);
    });
    it('should be able to reach end state', function() {
      assert.deepEqual(f.deltaStar(State('A'), '000'), State('B'));
      assert.deepEqual(f.deltaStar(State('A'), '1'), State('A'));
      assert.deepEqual(f.deltaStar(State('B'), '0010'), State('A'));
      assert.deepEqual(f.deltaStar(State('B'), '011010'), State('A'));
    });
    it('should return null when transition doesn\'t exist', function() {
      assert.isNull(f.deltaStar(State('C'), '011'));
    });
  });
  
  describe('#addAcceptStates', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
    });
    it('should be able to add one accept state', function() {
      f.addAcceptStates(State('A'));
      let i = 0;
      // TODO: Make a public method to get accept states.
      for (let accept_state of f._accept) {
        i++;
        assert.property(JSON.parse(accept_state), 'name');
      }
      assert.equal(i, 1);
    });
    it('should be able to add multiple accept states', function() {
      f.addAcceptStates(State('A'), State('B'));
      let i = 0;
      // TODO: Make a public method to get accept states.
      for (let accept_state of f._accept) {
        i++;
        assert.property(JSON.parse(accept_state), 'name');
      }
      assert.equal(i, 2);
    });
  });
  
  describe('#removeAcceptStates', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.addAcceptStates(State('A'), State('B'));
    });
    it('should be able to remove one accept state', function() {
      f.removeAcceptStates(State('A'));
      assert.equal(f._accept.size, 1);
    });
    it('should be able to remove multiple accept states', function() {
      f.removeAcceptStates(State('A'), State('B'));
      assert.equal(f._accept.size, 0);
    });
  });
  
  
  describe('#accepts', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('A'), '1', State('A'));
      f.setTransition(State('B'), '0', State('A'));
      f.setTransition(State('B'), '1', State('B'));
      f.setInitialState(State('A'));
      f.addAcceptStates(State('A'));
      // Only accepts strings even number of '0'.
    });
    it('should throw error when string contains a non-alphabet symbol', function() {
      assert.throws(() => f.accepts('010z'), "Symbol 'z' not in alphabet");
      assert.doesNotThrow(() => f.accepts('100'), /Symbol.*not in alphabet/);
    });
    it('should accept test-strings', function() {
      assert.ok(f.accepts('00'));
      assert.ok(f.accepts('1'));
      assert.ok(f.accepts('00100'));
      assert.ok(f.accepts('0110100'));
    });
    it('should not accept test-strings', function() {
      assert.notOk(f.accepts('000'));
      assert.notOk(f.accepts('01'));
      assert.notOk(f.accepts('0100'));
      assert.notOk(f.accepts('011010'));
    });
  });
  
  describe('#findReachableStates', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('B'), '1', State('B'));
      f.setInitialState(State('A'));
    });
    it('should return an iterator', function() {
      assert.property(f.findReachableStates(), Symbol.iterator);
    });
    it('should return reachable states', function() {
      assert.deepEqual([...f.findReachableStates()], [State('A'), State('B')]);
    });
    it('should not return unreachable states', function() {
      for (let state of f.findReachableStates()) {
        assert.notDeepEqual(state, State('C'));
      }
    });
  });
  
  describe('#removeUnreachableStates', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('B'), '1', State('B'));
      f.setTransition(State('C'), '1', State('C'));
      f.setTransition(State('C'), '0', State('A'));
      f.setInitialState(State('A'));
    });
    it('should return a new FA', function() {
      let m1 = f.removeUnreachableStates();
      assert.notEqual(f, m1);
    });
    it('should not modify original FA', function() {
      let m1 = f.removeUnreachableStates();
      assert.deepEqual([...f.states()], [State('A'), State('B'), State('C')]);
    });
    it('should only contain reachable states', function() {
      let m1 = f.removeUnreachableStates();
      assert.deepEqual([...m1.states()], [State('A'), State('B')]);
    });
    it('should not contain unreachable states', function() {
      let m1 = f.removeUnreachableStates();
      for (let state of m1.states()) {
        assert.notDeepEqual(state, State('C'));
      }
    });
    it('should contain transitions from reachable states', function() {
      let m1 = f.removeUnreachableStates();
      for (let [{state, char}, toState] of m1.transitions()) {
        assert.property(state, 'name');
      }
    });
    it('should not contain transitions from unreachable states', function() {
      let m1 = f.removeUnreachableStates();
      for (let [{state, char}, toState] of m1.transitions()) {
        assert.notDeepEqual(state, State('C'));
        assert.notDeepEqual(toState, State('C'));
      }
    });

  });

  describe('#clone', function() {
    let f, m1;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('B'), '1', State('B'));
      f.setTransition(State('C'), '1', State('C'));
      f.setInitialState(State('A'));
      m1 = f.clone();
    });
    it('should return a new FA', function() {
      assert.notEqual(f, m1);
    });
    it('should contain same states', function() {
      assert.deepEqual([...m1.states()], [...f.states()]);
    });
    it('should contain same transitions', function() {
      assert.deepEqual([...m1.transitions()], [...f.transitions()]);
    });
    it('should contain same accept states', function() {
      assert.deepEqual([...m1._accept], [...f._accept]);
    });
    it('should contain same initial state', function() {
      assert.deepEqual(m1.initialState(), f.initialState());
    });
    it('should contain same alphabet', function() {
      assert.ok(m1._alphabet.equals(f._alphabet));
    });
    it('should not be a shallow copy of states', function() {
      f.addStates(State('N'));
      assert.notDeepEqual([...m1.states()], [...f.states()]);
    });
    it('should not be a shallow copy of transitions', function() {
      f.setTransition(State('A'), '0', State('C'));
      assert.notDeepEqual([...m1.transitions()], [...f.transitions()]);
    });
  });
  
  describe('#complement', function() {
    let f, m1;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'), State('C'));
      f.setTransition(State('A'), '0', State('B'));
      f.setTransition(State('B'), '1', State('B'));
      f.setTransition(State('C'), '1', State('C'));
      f.setInitialState(State('A'));
      m1 = f.complement();
    });
    it('should return a new FA', function() {
      assert.notEqual(f, m1);
      assert.instanceOf(m1, FA);
    });
    it('should contain same states', function() {
      assert.deepEqual([...m1.states()], [...f.states()]);
    });
    it('should contain same transitions', function() {
      assert.deepEqual([...m1.transitions()], [...f.transitions()]);
    });
    it('should not contain same accept states', function() {
      assert.notDeepEqual([...m1._accept], [...f._accept]);
    });
    it('should have new # of accept states by #states-#accept', function() {
      let old_accept = f._accept.size;
      let new_accept = m1._accept.size;
      let states = m1.numberOfStates();
      assert.equal(new_accept, states-old_accept);
      
      f.addAcceptStates(State('A'));
      m1 = f.complement();
      old_accept = f._accept.size;
      new_accept = m1._accept.size;
      states = m1.numberOfStates();
      assert.equal(new_accept, states-old_accept);
    });
  });
  
  describe('Product of FA\'s', function() {
    let f, m1, m2;
    beforeEach(function() {
      let alphabet = new Alphabet('01');
      // FA which accepts strings that ends with '01'
    	m1 = new FA(alphabet);
    	m1.addStates(State("A"), State("B"), State("C"));
    	m1.setInitialState(State("A"));
    	m1.addAcceptStates(State("C"));

    	m1.setTransition(State("A"), '0', State("B"));
    	m1.setTransition(State("A"), '1', State("A"));
    	m1.setTransition(State("B"), '0', State("B"));
    	m1.setTransition(State("B"), '1', State("C"));
    	m1.setTransition(State("C"), '0', State("B"));
    	m1.setTransition(State("C"), '1', State("A"));

      m1.checkWellDefined();
      m1 = m1.removeUnreachableStates();
      
      // FA which accepts strings that have an odd length
    	m2 = new FA(alphabet);
    	m2.addStates(State("R"), State("S"));
    	m2.setInitialState(State("R"));
    	m2.addAcceptStates(State("S"));

    	m2.setTransition(State("R"), '0', State("S"));
    	m2.setTransition(State("R"), '1', State("S"));
    	m2.setTransition(State("S"), '0', State("R"));
    	m2.setTransition(State("S"), '1', State("R"));

      m2.checkWellDefined();
      m2 = m2.removeUnreachableStates();

      f = m1._product(m2, undefined);
    });
    
    it('should return a new FA', function() {
      assert.notEqual(f, m1);
      assert.notEqual(f, m2);
      assert.instanceOf(f, FA);
    });
    it('should contain combined states', function() {
      assert.deepEqual([...f.states()].sort(), [State('AR'), State('AS'), State('BR'), State('BS'), State('CR'), State('CS')].sort());
    });
    it('should contain combined transitions', function() {
      let i = 0;
      for (let [{state, char}, toState] of f.transitions()) {
        if (state.name == 'AR') {
          if (char == '0') {
            i++;
            assert.propertyVal(toState, 'name', 'BS');
          } else if (char == '1') {
            i++;
            assert.propertyVal(toState, 'name', 'AS');
          }
        } else if (state.name == 'CS' && char == '1') {
          i++;
          assert.propertyVal(toState, 'name', 'AR');
        }
      }
      assert.equal(i, 3);
    });
    
    describe('#intersection', function() {
      beforeEach(function() {
        f = m1.intersection(m2);
      });
      it('should only accept strings which are accepted in both FA\'s', function() {
        assert.equal(JSON.stringify(State('CS')), ...f._accept);
        assert.ok(m1.accepts('001'));
        assert.ok(m2.accepts('001'));
        assert.ok(f.accepts('001'));
        
        assert.ok(m1.accepts('01'));
        assert.notOk(m2.accepts('01'));
        assert.notOk(f.accepts('01'));

        assert.notOk(m1.accepts('110'));
        assert.ok(m2.accepts('110'));
        assert.notOk(f.accepts('110'));
      });
    });
    
    describe('#union', function() {
      beforeEach(function() {
        f = m1.union(m2);
      });
      it('should only accept strings which are accepted in both FA\'s', function() {
        let union_accept_states = ['AS', 'BS', 'CR', 'CS'], i = 0;
        for (let state of f._accept) {
          assert.equal(JSON.stringify(State(union_accept_states[i++])), state);
        }
        assert.ok(m1.accepts('001'));
        assert.ok(m2.accepts('001'));
        assert.ok(f.accepts('001'));
        
        assert.ok(m1.accepts('01'));
        assert.notOk(m2.accepts('01'));
        assert.ok(f.accepts('01'));

        assert.notOk(m1.accepts('110'));
        assert.ok(m2.accepts('110'));
        assert.ok(f.accepts('110'));
        
        assert.notOk(m1.accepts('1100'));
        assert.notOk(m2.accepts('1100'));
        assert.notOk(f.accepts('1100'));
      });
    });
    
    describe('#minus', function() {
      beforeEach(function() {
        f = m1.minus(m2);
      });
      it('should only accept strings which are accepted in both FA\'s', function() {
        assert.equal(JSON.stringify(State('CR')), ...f._accept);
        assert.ok(m1.accepts('001'));
        assert.ok(m2.accepts('001'));
        assert.notOk(f.accepts('001'));
        
        assert.ok(m1.accepts('01'));
        assert.notOk(m2.accepts('01'));
        assert.ok(f.accepts('01'));

        assert.notOk(m1.accepts('110'));
        assert.ok(m2.accepts('110'));
        assert.notOk(f.accepts('110'));
        
        assert.notOk(m1.accepts('1100'));
        assert.notOk(m2.accepts('1100'));
        assert.notOk(f.accepts('1100'));
      });
    });
  });
  
  describe('#checkWellDefined', function() {
    let f;
    beforeEach(function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'), State('B'));
    	f.setTransition(State("A"), '0', State("B"));
    	f.setTransition(State("A"), '1', State("B"));
    	f.setTransition(State("B"), '0', State("A"));
    	f.setTransition(State("B"), '1', State("A"));
      f.setInitialState(State('A'));
    });
    it('should check that no state, transition is null', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'Invalid null pointer');
      f.setInitialState(null);
      assert.throws(() => { f.checkWellDefined() }, 'Invalid null pointer');
    });
    it('should check if initial state is in set of states', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'initial state is not in the state set');
      f.setInitialState(State('C'));
      assert.throws(() => { f.checkWellDefined() }, 'initial state is not in the state set');
    });
    it('should check if all accept states is in set of states', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'Not all accept states are in the state set');
      f.addAcceptStates(State('C'));
      assert.throws(() => { f.checkWellDefined() }, 'Not all accept states are in the state set');
    });
    it('should check if all states have a transition on all symbols', function() {
      f = new FA(new Alphabet('01'));
      f.addStates(State('A'));
    	f.setTransition(State('A'), '0', State('A'));
      f.setInitialState(State('A'));
      
      assert.throws(() => { f.checkWellDefined() }, 'Transition function is not total');
      f.setTransition(State('A'), '1', State('A'));
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'Transition function is not total');
    });
    it('should check if all transitions end in a state in the set of states', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'There is a transition to a state that is not in state set');
      f._transitions.set(JSON.stringify({state: State('B'), char: '0'}), State('C'));
      assert.throws(() => { f.checkWellDefined() }, 'There is a transition to a state that is not in state set');
    });
    it('should check if all transitions start in a state in the set of states', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'Transition refer to a state not in the state set');
      f._transitions.set(JSON.stringify({state: State('C'), char: '0'}), JSON.stringify(State('B')));
      assert.throws(() => { f.checkWellDefined() }, 'Transition refer to a state not in the state set');
    });
    it('should check if all transitions have a valid symbol', function() {
      assert.doesNotThrow(() => { f.checkWellDefined() }, 'Non-alphabet symbol appears in transitions');
      f.setTransition(State('A'), 'x', State('B'));
      assert.throws(() => { f.checkWellDefined() }, 'Non-alphabet symbol appears in transitions');
    });
  });
});
